var hx = require("hbuilderx");
const { getPlatform } = require('./src/utils');
// const { updateCompletionProvider } = require('./src/index');
const FittencodeCompletionProvider = require('./src/FittencodeCompletionProvider');
const DeepSeekCompletionProvider = require('./src/DeepSeekCompletionProvider');
const CustomCompletionProvider = require('./src/CustomCompletionProvider.js');

const allLanguages = [
	'ActionScript', 'Ada', 'ASM', 'ASP', 'AutoIt', 'Batch', 'C', 'Clojure', 'CMake', 'Cobol', 'CoffeeScript', 'C#', 'Dart',
	'Diff', 'D', 'Docker', 'Ejs', 'Fortran', 'F#', 'Go', 'Groovy', 'Handlebars', 'HLSL', 'INI', 'Inno', 'Java', 'Julia', 'LaTeX',
	'Lisp', 'Log', 'Lua', 'Matlab', 'Objective-C', 'Pascal', 'Perl', 'PowerShell', 'Pug', 'Python', 'Razor', 'R', 'Ruby', 'Rust',
	'Scheme', 'ShaderLab', 'SmallTalk', 'SQL', 'Swift', 'Tcl', 'VB', 'VeriLog', 'Vhdl', 'WXML', 'Yaml', 'CSS', 'HTML', 'JavaScript',
	'JSON', 'Less', 'Markdown', 'PHP', 'SCSS', 'Stylus', 'Text', 'TypeScript', 'Vue', 'XML', 'Kotlin', 'UTS'
]

const blackList = []

function arrayDifference (arr1, arr2) {
	return arr1.filter(item => !arr2.includes(item))
}

function getAllowLanguages () {
	const allowLanguages = arrayDifference(allLanguages, blackList)
	
	let items = []
	
	allowLanguages.map(item => {
		items.push({
			language: item
		})
	})
	
	return items
}

let currentProviderDisposable = null;
let provider = null;

function updateCompletionProvider(context, platform) {
	if (currentProviderDisposable) {
		currentProviderDisposable.dispose();
	}
	
	if (provider) {
		provider.hideStatusBarItem();
	}
	
	if (platform == 'fittencode') {
		provider = new FittencodeCompletionProvider(context);
	} else if (platform == 'deepseek') {
		provider = new DeepSeekCompletionProvider(context);
	} else if (platform == 'custom') {
		provider = new CustomCompletionProvider(context);
	}
	
	currentProviderDisposable = hx.languages.registerInlineCompletionItemProvider(
		getAllowLanguages(),
		{
			provideInlineCompletionItems: async (document, position, context, token) => {
				return provider.provideInlineCompletionItems(document, position, context, token)
				// return new FittenCodeCompletionProvider(_context).provideInlineCompletionItems(document, position, context, token)
			}
		}
	)
	
	context.subscriptions.push(currentProviderDisposable)
}

//该方法将在插件激活的时候调用
function activate(context) {
	const platform = getPlatform();
	updateCompletionProvider(context, platform);
	hx.workspace.onDidChangeConfiguration((e) => {
		if (e.affectsConfiguration('kux.copilot.platform')) {
			const newPlatform = getPlatform();
			updateCompletionProvider(context, newPlatform);
		}
	})
}
//该方法将在插件禁用的时候调用（目前是在插件卸载的时候触发）
function deactivate() {

}
module.exports = {
	activate,
	deactivate
}