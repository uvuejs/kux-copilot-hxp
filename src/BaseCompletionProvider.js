const vscode = require('vscode');
const hx = require('hbuilderx');
const FittencodeCompletionProvider = require("./FittencodeCompletionProvider");
const DeepSeekCompletionProvider = require('./DeepSeekCompletionProvider');

class BaseCompletionProvider {
	#platform = '';
	#context = null;
	
	constructor(context) {
		const config = hx.workspace.getConfiguration();
		this.context = context;
		this.platform = config.get('kux.copilot.platform');
		hx.workspace.onDidChangeConfiguration((event) => {
			if (event.affectsConfiguration('kux.copilot.platform')) {
				this.platform = config.get('kux.copilot.platform');
			}
		});
	}
	
	requestCompletion(document, position, context, token) {
		console.log(this.platform);
		if (this.platform == 'fittencode') {
			return new FittencodeCompletionProvider(this.context).requestCompletion(document, position, context, token);
		}
		
		if (this.platform == 'deepseek') {
			return new DeepSeekCompletionProvider(this.context).requestCompletion(document, position, context, token);
		}
	}
}

module.exports = BaseCompletionProvider;