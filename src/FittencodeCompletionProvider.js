const hx = require('hbuilderx')
const axios = require('axios')
const vscode = require('vscode');
const path = require('path');
const { debounce } = require('lodash');
// const vscode = require('vscode')
const { getCodeContext, isUndoEvent, getProjectContext } = require('./utils');

const axiosInstance = axios.create({
	baseURL: 'https://fc.fittenlab.cn',
	// headers: {
	//   'Accept': 'text/event-stream'
	// },
	// responseType: 'stream'
});

async function sendCompletionRequest(params) {
	const completionUrl =
		`https://fc.fittenlab.cn/codeapi/completion/generate_one_stage/abc123?ide=hbuilderx&v=0.2.1`
	return axiosInstance.post(completionUrl, params)
}

function escapePrompt(prompt) {
	// 替换特殊字符
	let escapedPrompt = prompt.replace(/"/g, '\\"'); // 替换双引号
	escapedPrompt = escapedPrompt.replace(/\\\\n/g, '\\n'); // 替换换行符
	escapedPrompt = escapedPrompt.replace(/\t/g, '\\t'); // 替换制表符
	return escapedPrompt;
}

class FittencodeCompletionProvider {
	#debouncedProvideInlineCompletionItems = null;
	#config = null;
	#apiKey = '';
	#statusBarItem = null; // 状态栏
	#isUndo = false; // 记录是否是撤销操作
	#debounceTimeout = null;
	#cache = new Map();
	#previousPrefix = '';
	
	constructor(context) {
		this.config = hx.workspace.getConfiguration();
		this.apiKey = this.config.get('kux.copilot.apikey');
		hx.workspace.onDidChangeConfiguration((event) => {
			if (event.affectsConfiguration('kux.copilot.apikey')) {
				// console.log('配置变化了', config.get('kux.copilot.apikey'));
				this.apiKey = this.config.get('kux.copilot.apikey');
				// this.createOpenAI();
			}
		})
		// 监听文档变化
		hx.workspace.onDidChangeTextDocument((event) => {
			this.isUndo = isUndoEvent(event);
		});
		// 获取扩展目录
		const extensionPath = context.extensionPath;
		// 获取自定义图标路径
		const iconPath = vscode.Uri.file(path.join(extensionPath, 'resources', 'logo.png'));
		// 初始化状态栏
		hx.window.clearStatusBarMessage();
		this.#statusBarItem = hx.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 100);
		this.#statusBarItem.text = '【kux-copilot:fittencode】插件已加载完毕';
		// this.#statusBarItem.iconPath = iconPath.path;
		this.#statusBarItem.show();
		// 防抖处理补全请求
		this.debouncedProvideInlineCompletionItems = debounce(this.provideInlineCompletionItems, 300);
	}
	
	finish() {
		if (this.#statusBarItem) {
			this.#statusBarItem.text = '【kux-copilot:fittencode】插件已加载完毕';
		}
		this.isRequestCompletion = false;
	}
	
	showErrorDebounced(error) {
		debounce((error) => {
			hx.window.showErrorMessage("【kux-copilot】请求失败");
		}, 3000)
	}
	
	async provideInlineCompletionItems(document, position, context, token) {
		if (this.isUndo) {
			this.finish();
			return new vscode.CompletionList([]);
		}
		
		if (this.debounceTimeout) {
			clearTimeout(this.debounceTimeout);
		}
		
		this.#statusBarItem.text = '【kux-copilot:fittencode】正在请求补全...'
		
		const { prefix, suffix } = getCodeContext(document, position);
		
		if (prefix === this.#previousPrefix) {
			const cacheKey = `${prefix}-${suffix}`;
			if (this.#cache.has(cacheKey)) {
				return this.#cache.get(cacheKey);
			}
		}
		
		this.#previousPrefix = prefix;
		
		const result = await this.requestCompletion(document, position, context, token);
		
		const cacheKey = `${prefix}-${suffix}`;
		this.#cache.set(cacheKey, result);
		
		return result;
	}
	
	async requestCompletion(document, position, context, token) {
		const line = await document.lineAt(position)
		const filename = document.fileName
		if (position.line == 0 && position.character == 0) return { items: [] }
		const lineNum = position.line + 1
		const colNum = position.character + 1
		const text = document.getText()
		const lines = text.split('\n')
		// const prefix = lines.slice(0, lineNum).join('\n')
		// const suffix = lines.slice(lineNum).join('\n')
		const { prefix, suffix } = getCodeContext(document, position);
		
		let newPrefix = '';
		// 组装请求参数
		if (this.config?.get('kux.copilot.provideProjectContext') == true) {
			// 获取项目上下文
			const projectContext = getProjectContext(document);
			newPrefix = projectContext + prefix;
		} else {
			newPrefix = prefix;
		}
		
		const prompt = `!FCPREFIX!${newPrefix}!FCSUFFIX!${suffix}!FCMIDDLE!`
		const escapedPrompt = escapePrompt(prompt)
		
		const params = {
			inputs: prompt,
			meta_datas: {
				filename
			}
		}
					
		try {
			const response = await sendCompletionRequest(params)
			// console.log(response);
			const item = new vscode.CompletionItem(
				'FittenCode Suggestion',
				vscode.CompletionItemKind.Text
			);
			item.insertText = response.data.generated_text ?? '';
			item.insertText = item.insertText
				.replace(/<\|endoftext\|>/g, '')
			
			item.detail = 'FittenCode Completion';
			return new vscode.CompletionList([item]);
		} catch (error) {
			this.showErrorDebounced(error);
			return new vscode.CompletionList([]);
		} finally {
			this.finish();
		}
	}
	
	hideStatusBarItem() {
		this.#statusBarItem.hide();
		this.#statusBarItem = null;
	}
}

module.exports = FittencodeCompletionProvider