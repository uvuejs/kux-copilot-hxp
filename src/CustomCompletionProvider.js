const hx = require('hbuilderx');
const axios = require('axios');
const vscode = require('vscode');
const path = require('path');
const { debounce: _debounce } = require('lodash');
const { OpenAI } = require('openai');
const { debounce, getCodeContext, isUndoEvent, getProjectContext, getRootConfig } = require('./utils');

class CustomCompletionProvider {
	#openaiInstance = null;
	#apiKey = '';
	#lastDocumentState = null; // 记录上一次文档状态
	#isUndo = false; // 记录是否是撤销操作
	#statusBarItem = null; // 状态栏
	#completionList = null;
	#config = null;
	#debouncedProvideInlineCompletionItems = null;
	#isRequestCompletion = false;
	#cache = new Map();
	#previousPrefix = '';
	#baseUrl = '';
	#model = 'deepseek-coder';
	
	constructor(context) {
		this.config = getRootConfig();
		this.apiKey = this.config.get('kux.copilot.apikey');
		this.baseUrl = this.config.get('kux.copilot.baseURL');
		this.model = this.config.get('kux.copilot.model') ?? 'deepseek-coder';
		this.createOpenAI();
		hx.workspace.onDidChangeConfiguration((event) => {
			if (event.affectsConfiguration('kux.copilot.apikey')) {
				// console.log('配置变化了', config.get('kux.copilot.apikey'));
				this.config = getRootConfig();
				this.apiKey = this.config.get('kux.copilot.apikey');
				this.createOpenAI();
			} else if (event.affectsConfiguration('kux.copilot.baseURL')) {
				this.config = getRootConfig();
				this.baseUrl = this.config.get('kux.copilot.baseURL');
				this.createOpenAI();
			} else if (event.affectsConfiguration('kux.copilot.model')) {
				this.config = getRootConfig();
				this.model = this.config.get('kux.copilot.model');
				this.createOpenAI();
			}
		})
		// 监听文档变化
		hx.workspace.onDidChangeTextDocument((event) => {
			this.isUndo = isUndoEvent(event);
		});
		// 获取扩展目录
		const extensionPath = context.extensionPath;
		// 获取自定义图标路径
		const iconPath = vscode.Uri.file(path.join(extensionPath, 'resources', 'logo.png'));
		// 初始化状态栏
		hx.window.clearStatusBarMessage();
		this.#statusBarItem = hx.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 100);
		this.#statusBarItem.text = '【kux-copilot:custom】插件已加载完毕';
		// this.#statusBarItem.iconPath = iconPath.path;
		this.#statusBarItem.show();
		// 防抖处理补全请求
		this.debouncedProvideInlineCompletionItems = _debounce(this.provideInlineCompletionItems, 300);
	}
	
	createOpenAI() {
		this.openaiInstance = new OpenAI({
			apiKey: this.apiKey,
			baseURL: this.baseUrl
		});
	}
	
	showErrorDebounced(error) {
		debounce((error) => {
			hx.window.showErrorMessage("【kux-copilot】请求失败:" + error.message);
		}, 3000)
	}
	
	finish() {
		if (this.#statusBarItem) {
			this.#statusBarItem.text = '【kux-copilot:custom】插件已加载完毕';
		}
		this.isRequestCompletion = false;
	}
	
	async provideInlineCompletionItems(document, position, context, token) {
		// // 判断触发补全请求的方式
		// if (this.isRequestCompletion) {
		// 	this.finish();
		// 	return new vscode.CompletionList([{insertText: '请求中补全'}]);
		// }
		
		// this.isRequestCompletion = true;
		
		// if (context.triggerKind === vscode.CompletionTriggerKind.Invoke) {
		// 	this.finish();
		// 	return new vscode.CompletionList([{insertText: '自动补全'}]);
		// }
		if (this.isUndo) {
			this.finish();
			return new vscode.CompletionList([]);
		}
		
		this.#statusBarItem.text = '【kux-copilot:custom】正在请求补全...'
		
		let prompt = '';
		// 获取当前代码上下文
		const { prefix, suffix } = getCodeContext(document, position);
		
		if (prefix === this.#previousPrefix) {
			const cacheKey = `${prefix}-${suffix}`;
			if (this.#cache.has(cacheKey)) {
				return this.#cache.get(cacheKey);
			}
		}
		
		this.#previousPrefix = prefix;
		
		const result = await this.requestCompletion(document, position, context, token);
		
		const cacheKey = `${prefix}-${suffix}`;
		this.#cache.set(cacheKey, result);
		
		return result;
	}
	
	async requestCompletion(document, position, context, token) {
		// return this.debouncedProvideInlineCompletionItems(document, position, context, token);
		let prompt = '';
		// 获取当前代码上下文
		const { prefix, suffix } = getCodeContext(document, position);
		// 组装请求参数
		if (this.config?.get('kux.copilot.provideProjectContext') == true) {
			// 获取项目上下文
			const projectContext = getProjectContext(document);
			prompt = projectContext + prefix;
		} else {
			prompt = prefix;
		}
		try {
			const completion = await this.openaiInstance?.completions?.create({
				model: this.model,
				prompt: prompt,
				echo: false,
				frequency_penalty: 0,
				logprobs: 0,
				max_tokens: 1024,
				presence_penalty: 0,
				stop: null,
				stream: false,
				stream_options: null,
				suffix: suffix,
				temperature: 1,
				top_p: 1
			});
			
			const suggestions = completion.choices.map(choice => {
				const item = new vscode.CompletionItem(
					'KuxCustom Suggestion',
					vscode.CompletionItemKind.Text
				);
				item.insertText = choice.text;
				item.detail = 'KuxCustom FIM Completion';
				return item;
			})
			
			const items = new vscode.CompletionList(suggestions);
			
			return items;
			// for await (const text of completion) {
			// 	// console.log(choice);
			// 	const choice = text.choices[0];
			// 	if (choice.text) {
			// 		const item = new vscode.CompletionItem(
			// 			'DeepSeek Suggestion',
			// 			vscode.CompletionItemKind.Text
			// 		);
			// 		item.insertText = choice.text;
			// 		item.detail = 'DeepSeek FIM Completion';
					
			// 		// console.log(item);
					
			// 		this.completionList.items.push(item);
			// 	}
			// }
			
			// console.log(this.completionList);
			
			// return this.completionList;
		} catch (error) {
			// this.showErrorDebounced(error);
			hx.window.showErrorMessage("【kux-copilot】请求失败:" + error.message);
			return new vscode.CompletionList([]);
		} finally {
			this.finish();
		}
	}
	
	hideStatusBarItem() {
		this.#statusBarItem.hide();
		this.#statusBarItem = null;
	}
}

module.exports = CustomCompletionProvider;