const vscode = require('vscode');
const hx = require('hbuilderx');
const fs = require('fs');
const path = require('path');
const babelParser = require('@babel/parser');

function debounce(func, delay) {
	let debounceTimer;
	
	return (...args) => {
		if (debounceTimer) {
			clearTimeout(debounceTimer)
		}
		debounceTimer = setTimeout(() => func.apply(this, args), delay);
	}
}

function getCodeContext(document, position) {
	// 获取当前代码上下文
	const prefix = document.getText(new vscode.Range(
		new vscode.Position(0, 0),
		position
	));
	const suffix = document.getText(new vscode.Range(
		position,
		document.positionAt(document.getText().length)
	));
	
	return {
		prefix,
		suffix
	}
}

function extractImports(fileName) {
	const fileContent = fs.readFileSync(fileName, 'utf-8');
	const ast = babelParser.parse(fileContent, {
		sourceType: 'module',
		plugins: ['typescript', 'jsx']
	});
	const imports = [];
	ast.program.body.forEach(node => {
		if (node.type === 'ImportDeclaration') {
			imports.push(node.source.value);
		}
	});
	return imports;
}

function findProjectRoot(fileName) {
	const workspaceFolders = hx.workspace.workspaceFolders;
	if (workspaceFolders && workspaceFolders.length > 0) {
		// 获取第一个工作区文件夹的路径
		const rootPath = workspaceFolders[0].uri.fsPath;
		return rootPath;
	}
	return null; // 如果没有工作区文件夹，返回 null
}

function extractProjectConfig(fileName) {
	const projectRoot = findProjectRoot(fileName);
	const packageJsonPath = path.join(projectRoot, 'package.json');
	
	if (fs.existsSync(packageJsonPath)) {
		const packageJson = JSON.parse(fs.readFileSync(packageJsonPath, 'utf-8'));
		const dependencies = Object.keys(packageJson.dependencies || {});
		const devDependencies = Object.keys(packageJson.devDependencies || {});
		
		let context = '# 项目配置: \n';
		context += `依赖: \n${dependencies.join('\n')}\n`;
		context += `开发依赖: \n${devDependencies.join('\n')}\n`;
		
		return context;
	}
	
	return '# 项目配置: 没有找到 package.json\n';
}

function extractTsConfig(fileName) {
	const projectRoot = findProjectRoot(fileName);
	const tsConfigPath = path.join(projectRoot, 'tsconfig.json');
	
	if (fs.existsSync(tsConfigPath)) {
		const tsConfig = JSON.parse(fs.readFileSync(tsConfigPath, 'utf-8'));
		const compilerOptions = tsConfig.compilerOptions || {};
		const context = '# tsconfig.json: \n';
		context += JSON.stringify(compilerOptions, null, 2);
		return context;
	}
	
	return '# tsconfig.json: 没有找到 tsconfig.json\n';
}

function getProjectContext(document) {
	const fileName = document.fileName;
	const languageId = document.languageId;
	
	let context = '';
	
	// 分析导入的模块
	if (languageId === 'typescript' || languageId === 'javascript' || languageId === 'uts') {
		context += '# 编程语言: \n';
		context += languageId + '\n';
		const imports = extractImports(fileName);
		context += '# 导入的模块: \n';
		context += imports.join('\n') + '\n';
	}
	
	// 分析项目配置
	context += extractProjectConfig(fileName);
	// 分析 tsconfig.json
	context += extractTsConfig(fileName);
	
	return context;
}

function isRangesEqual(range1, range2) {
	console.log(range1, range2);
	return (
		range1.start.line === range2.start.line &&
		range1.start.character === range2.start.character &&
		range1.end.line === range2.end.line &&
		range1.end.character === range2.end.character
	);
}

function isUndoEvent(event) {
	return event.contentChanges.some(change => {
		return change.text === "" && change.rangeLength > 0;
	})
}

function getPlatform() {
	const config = hx.workspace.getConfiguration();
	return config.get('kux.copilot.platform', 'fittencode');
}

function getRootConfig() {
	const config = hx.workspace.getConfiguration();
	return config;
}

module.exports = {
	debounce,
	getCodeContext,
	isUndoEvent,
	getProjectContext,
	getPlatform,
	getRootConfig
}