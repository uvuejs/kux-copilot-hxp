## 简介
该插件是一个基于国产大模型的代码编程助手，可以快速实现代码的生成与补全，自动根据注释生成代码，帮助开发者提高工作效率。支持hx兼容的所有编程语言。

## 功能
+ ### 代码生成与补全
	在打开文件输入代码时，会从当前光标开始生成代码和补全代码，在未接受时代码灰色显示，按 `tab` 按键即可快速插入自动生成的代码；按 `ESC` 按键即为拒绝接受，此时会取消生成的代码建议。
	
+ ### 注释生成代码
	在打开文件输入标准注释时，会从当前光标开始生成代码，在未接受时代码灰色显示，按 `tab` 按键即可快速插入自动生成的代码；按 `ESC` 按键即为拒绝接受，此时会取消生成的代码建议。

## 注意
目前支持了 [fittencode](https://code.fittentech.com/) 大模型和 [deepseek](https://platform.deepseek.com/) 大模型。

+ 【fittencode】不需要登录，插件配置的 `apikey` 是可选项。
+ 【deepseek】：
	+ 需要提供自己的 `apikey`。[deepseek开放平台](https://platform.deepseek.com/api_keys)
+ 【custom】：`v1.0.2` 及以上版本支持选择自定义提供方。
	+ 在【插件配置】填写自定义 `baseURL`，支持任何兼容 `openai` 标准的API服务，具体API标准如下：
	
		+ 【GET】/xxx/models
		+ 【POST】/xxx/chat/completions
		+ 【POST】/xxx/completions
		+ 【POST】/xxx/embeddings

	+ 在【插件配置】填写自定义 `model`，默认是 `deepseek-coder`。建议选择代码补齐专属模型。